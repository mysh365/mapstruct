package cn.gthome.www.enums;

/**
 * @author gj
 * @create 2021-11-29-14:22
 */
public enum Gender {

    Male("1","男"),
    Female("0","女");

    private String code ;

    private String name ;

    Gender(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
