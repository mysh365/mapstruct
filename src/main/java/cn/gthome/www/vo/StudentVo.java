package cn.gthome.www.vo;

import cn.gthome.www.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author gj
 * @create 2021-11-29-14:20
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentVo {
    private String name ;

    private int age ;

    private String gender;

    private double height;

    private String birthday;

}
