package cn.gthome.www.mapper;

import cn.gthome.www.entity.Student;
import cn.gthome.www.vo.StudentVo;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author gj
 * @create 2021-11-29-14:27
 */
@Mapper
public interface StudentMapper {
    /**
     * student 转 studentVo对象
     *
     * @param student
     * @return
     */
    @Mapping(source = "gender.name",target = "gender")
    @Mapping(source = "birthday",target = "birthday",dateFormat = "yyyy-MM-dd HH:mm:ss")
    StudentVo student2StudentVo(Student student);

    /**
     * students 转换成 studentVos
     * @param students
     * @return
     */
    List<StudentVo> students2StudentVos(List<Student> students);
}
