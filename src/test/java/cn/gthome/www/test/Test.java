package cn.gthome.www.test;

import cn.gthome.www.entity.Student;
import cn.gthome.www.enums.Gender;
import cn.gthome.www.mapper.StudentMapper;
import cn.gthome.www.vo.StudentVo;
import org.mapstruct.factory.Mappers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author gj
 * @create 2021-11-29-14:32
 */
public class Test {
    public static void main(String[] args) {
        Student student = Student.builder()
                .name("xiaoming")
                .age(6)
                .gender(Gender.Male)
                .height(121.1)
                .birthday(new Date()).build();
        System.out.println(student);

        StudentMapper studentMapper = Mappers.getMapper(StudentMapper.class);
        StudentVo studentVo = studentMapper.student2StudentVo(student);
        System.out.println(studentVo);


        System.out.println("-------------------------");

        List<Student> stus = new ArrayList<>();
        stus.add(student);
        List<StudentVo> studentVos = studentMapper.students2StudentVos(stus);
        System.out.println(studentVos);

    }
}
